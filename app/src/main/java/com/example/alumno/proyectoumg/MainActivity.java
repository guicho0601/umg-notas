package com.example.alumno.proyectoumg;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {

    public static String URL_HOST = "http://192.168.43.94/umg_notas/public/";

    EditText etEmail, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
    }

    public void cancelar(View view){
        etEmail.setText("");
        etPassword.setText("");
    }
    public void ingresar(View view){
        Login login = new Login(etEmail.getText().toString(), etPassword.getText().toString());
        login.execute();
    }

    private class Login extends AsyncTask<Void, Void, Respuesta> {

        private final String correo, password;

        Login(String correo, String password){
            this.correo = correo;
            this.password = password;
        }

        @Override
        protected Respuesta doInBackground(Void... voids) {
            RestClient client = new RestClient(MainActivity.URL_HOST + "login");
            client.AddParam("correo", correo);
            client.AddParam("password", password);

            try {
                client.Execute("POST");
            } catch (Exception e) {
                e.printStackTrace();
            }

            String response = client.getResponse();
            Gson gson = new Gson();
            return gson.fromJson(response, Respuesta.class);
        }

        @Override
        protected void onPostExecute(Respuesta respuesta) {
            super.onPostExecute(respuesta);
            if(respuesta.isRespuesta()){
                Intent intent = new Intent(MainActivity.this, InfoUsuarioActivity.class);
                intent.putExtra("usuario", new Gson().toJson(respuesta.getUsuario()));
                startActivity(intent);
            }else{
                Toast.makeText(MainActivity.this, respuesta.getMensaje_error(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
