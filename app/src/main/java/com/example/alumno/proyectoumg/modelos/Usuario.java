package com.example.alumno.proyectoumg.modelos;

/**
 * Created by Luis Ramos on 26/07/17.
 */

public class Usuario {
    private final int id;
    private final String nombre, apellido, correo;

    public Usuario(int id, String nombre, String apellido, String correo) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getCorreo() {
        return correo;
    }
}
