package com.example.alumno.proyectoumg;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alumno.proyectoumg.modelos.Asignatura;
import com.example.alumno.proyectoumg.modelos.Usuario;
import com.google.gson.Gson;

import java.util.List;

public class NotasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notas);
        Gson gson = new Gson();
        Respuesta respuesta = gson.fromJson(getIntent().getStringExtra("respuesta"), Respuesta.class);
        Usuario usuario = gson.fromJson(getIntent().getStringExtra("usuario"), Usuario.class);
        RecyclerView rv1 = (RecyclerView) findViewById(R.id.rv1);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv1.setLayoutManager(mLayoutManager);
        rv1.setItemAnimator(new DefaultItemAnimator());
        RVAdapter adapter = new RVAdapter(respuesta.getAsignaturas());
        rv1.setAdapter(adapter);
        if(respuesta.getAsignaturas().size() == 0){
            Toast.makeText(this, "No tiene ningún curso asigando en este ciclo.", Toast.LENGTH_SHORT).show();
        }

        TextView tv1 = (TextView) findViewById(R.id.tv1);
        TextView tv2 = (TextView) findViewById(R.id.tv2);
        TextView tv3 = (TextView) findViewById(R.id.tv3);
        tv1.setText("Usuario: " + usuario.getNombre());
        tv2.setText("Año: " + getIntent().getIntExtra("anio", 2017));
        tv3.setText("Trimestre: " + getIntent().getIntExtra("trimestre", 1));
    }

    class RVAdapter extends RecyclerView.Adapter<RVAdapter.AsignaturasViewHolder>{

        private List<Asignatura> asignaturas;

        RVAdapter(List<Asignatura> asignaturas) {
            this.asignaturas = asignaturas;
        }

        @Override
        public AsignaturasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.nota_view, parent, false);
            return new AsignaturasViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(AsignaturasViewHolder holder, int position) {
            Asignatura asignatura = asignaturas.get(position);
            holder.nombreCurso.setText(asignatura.getCurso().getNombre());
            holder.nota.setText(String.valueOf(asignatura.getNota()));
        }

        @Override
        public int getItemCount() {
            return asignaturas.size();
        }

        class AsignaturasViewHolder extends RecyclerView.ViewHolder {
            TextView nombreCurso;
            TextView nota;

            AsignaturasViewHolder(View itemView) {
                super(itemView);
                nombreCurso = (TextView) itemView.findViewById(R.id.tvCurso);
                nota = (TextView) itemView.findViewById(R.id.tvNota);
            }
        }

    }
}
