package com.example.alumno.proyectoumg;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.alumno.proyectoumg.modelos.Usuario;
import com.google.gson.Gson;

public class InfoUsuarioActivity extends AppCompatActivity {

    private Usuario usuario;
    Spinner spCiclo, spAnyo;
    String[] itemsCiclo, itemsAnyo;
    ArrayAdapter<String> adapterCiclo, adapterAnyo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_usuario);
        String usuario_string = getIntent().getStringExtra("usuario");
        usuario = new Gson().fromJson(usuario_string, Usuario.class);

        /********************************CICLO********************************/
        itemsCiclo = new String[]{"1", "2", "3", "4"};
        adapterCiclo = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, itemsCiclo);
        spCiclo = (Spinner) findViewById(R.id.spCiclo);
        spCiclo.setAdapter(adapterCiclo);

        /********************************AÑO********************************/
        itemsAnyo = new String[]{"2015", "2016", "2017"};
        adapterAnyo = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, itemsAnyo);

        spAnyo = (Spinner) findViewById(R.id.spAnyo);
        spAnyo.setAdapter(adapterAnyo);
        Toast.makeText(this, "Bienvenido " + usuario.getNombre(), Toast.LENGTH_SHORT).show();
    }

    public void consultarNotas(View view){
        int anio = Integer.parseInt(spAnyo.getSelectedItem().toString());
        int trimestre = Integer.parseInt(spCiclo.getSelectedItem().toString());
        new ConsultaNotas(usuario.getId(), anio, trimestre).execute();
    }

    private class ConsultaNotas extends AsyncTask<Void, Void, Respuesta> {

        private final int usuario_id, anio, trimestre;

        ConsultaNotas(int usuario_id, int anio, int trimestre){
            this.usuario_id = usuario_id;
            this.anio = anio;
            this.trimestre = trimestre;
        }

        @Override
        protected Respuesta doInBackground(Void... voids) {
            RestClient client = new RestClient(MainActivity.URL_HOST + "notas/" + usuario_id + "/" + anio + "/" + trimestre);
            try {
                client.Execute("POST");
            } catch (Exception e) {
                e.printStackTrace();
            }

            String response = client.getResponse();
            Gson gson = new Gson();
            return gson.fromJson(response, Respuesta.class);
        }

        @Override
        protected void onPostExecute(Respuesta respuesta) {
            super.onPostExecute(respuesta);
            if(respuesta.isRespuesta()){
                Intent intent = new Intent(InfoUsuarioActivity.this, NotasActivity.class);
                Gson gson = new Gson();
                intent.putExtra("respuesta", gson.toJson(respuesta));
                intent.putExtra("usuario", gson.toJson(usuario));
                intent.putExtra("anio", anio);
                intent.putExtra("trimestre", trimestre);
                startActivity(intent);
            }else{
                Toast.makeText(InfoUsuarioActivity.this, respuesta.getMensaje_error(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
