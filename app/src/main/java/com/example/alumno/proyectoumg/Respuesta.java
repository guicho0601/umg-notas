package com.example.alumno.proyectoumg;

import com.example.alumno.proyectoumg.modelos.Asignatura;
import com.example.alumno.proyectoumg.modelos.Usuario;

import java.util.List;

/**
 * Created by Luis Ramos on 26/07/17.
 */

public class Respuesta {

    private final boolean respuesta;
    private final int codigo_error;
    private final String mensaje_error;
    private Usuario usuario;
    private List<Asignatura> asignaturas;

    public Respuesta(boolean respuesta, int codigo_error, String mensaje_error, Usuario usuario) {
        this.respuesta = respuesta;
        this.codigo_error = codigo_error;
        this.mensaje_error = mensaje_error;
        this.usuario = usuario;
    }

    public Respuesta(boolean respuesta, int codigo_error, String mensaje_error, List<Asignatura> asignaturas) {
        this.respuesta = respuesta;
        this.codigo_error = codigo_error;
        this.mensaje_error = mensaje_error;
        this.asignaturas = asignaturas;
    }

    public boolean isRespuesta() {
        return respuesta;
    }

    public int getCodigo_error() {
        return codigo_error;
    }

    public String getMensaje_error() {
        return mensaje_error;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public List<Asignatura> getAsignaturas() {
        return asignaturas;
    }
}
