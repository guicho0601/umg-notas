package com.example.alumno.proyectoumg.modelos;

/**
 * Created by Luis Ramos on 26/07/17.
 */

public class Asignatura {

    private final int id, usuario_id, curso_id, anio, trimestre, nota;
    private Curso curso;

    public Asignatura(int id, int usuario_id, int curso_id, int anio, int trimestre, int nota, Curso curso) {
        this.id = id;
        this.usuario_id = usuario_id;
        this.curso_id = curso_id;
        this.anio = anio;
        this.trimestre = trimestre;
        this.nota = nota;
        this.curso = curso;
    }

    public int getId() {
        return id;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public int getCurso_id() {
        return curso_id;
    }

    public int getAnio() {
        return anio;
    }

    public int getTrimestre() {
        return trimestre;
    }

    public int getNota() {
        return nota;
    }

    public Curso getCurso() {
        return curso;
    }
}
