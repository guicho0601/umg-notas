package com.example.alumno.proyectoumg.modelos;

/**
 * Created by Luis Ramos on 26/07/17.
 */

public class Curso {
    private final int id;
    private final String nombre;

    public Curso(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}
